class Canvas
  attr_reader :canvas, :m, :n

  def initialize(m, n)
    validate_numericality_of(:Width => m, :Height => n)
    validate_initial_range(m,n)
    @m = m
    @n = n
    @canvas = Array.new(n){Array.new(m, "O")}
  end

  def draw
    self.canvas.each do |row|
      puts row.join('')
    end 
  end

  def clear
    @canvas = Array.new(self.n){Array.new(self.m, "O")}
  end

  def rect(x1,y1,x2,y2,colour)
    validate_range_of(:x1 => x1, :y1 => y1, :x2 => x2, :y2 => y2)
    validate_colour(colour)
    y1, y2 = y2, y1 if y1 > y2
    x1, x2 = x2, x1 if x1 > x2
    y1.upto(y2) do |y|
      x1.upto(x2) do |x|
        do_pixel(x,y,colour)
      end
    end
  end

  def fill(x,y,colour)
    validate_range_of(:x => x, :y => y)
    validate_colour(colour)
    oldcolour = self.canvas[y-1][x-1]
    do_fill(x,y,colour,oldcolour)
  end

  # wrapper methods
  def pixel(x,y,colour)
    validate_range_of(:x => x, :y => y)
    validate_colour(colour)
    do_pixel(x,y,colour)
  end

  private
  def do_fill(x,y,colour,oldcolour)
    if self.canvas[y-1][x-1] == oldcolour
      do_pixel(x,y,colour)
      do_fill(x-1,y,colour,oldcolour) if (x-1).between?(1,self.m)
      do_fill(x+1,y,colour,oldcolour) if (x+1).between?(1,self.m)
      do_fill(x,y-1,colour,oldcolour) if (y-1).between?(1,self.n)
      do_fill(x,y+1,colour,oldcolour) if (y+1).between?(1,self.n)
    end
  end

  def do_pixel(x,y,colour)
    self.canvas[y-1][x-1] = colour 
  end

  def validate_colour(colour)
    raise ArgumentError, "colour must be a single capital letter" unless colour.is_a?(String) && (/^[A-Z]$/ =~ colour)
  end

  def validate_numericality_of(args = {})
    errors = []
    args.each_pair do |key, arg|
      errors << "#{key} must be an integer" unless arg.is_a?(Integer)
    end
    raise ArgumentError, errors.join(', ') unless errors.empty?
  end

  def validate_range_of(args = {})
    validate_numericality_of(args)
    errors = []
    args.each_pair do |key, arg|
      max = ((/x/=~key) ? self.m : ((/y/=~key) ? self.n : 1))
      errors << "#{key} must be between 1 and #{max}" unless arg.between?(1,max)
    end
    raise RangeError, errors.join(', ') unless errors.empty?
  end

  def validate_initial_range(m,n)
    errors = []
    errors << "m must be between 1 and 250" unless m.between?(1,250)
    errors << "n must be between 1 and 250" unless n.between?(1,250)
    raise RangeError, errors.join(', ') unless errors.empty?
  end
end
