require 'minitest/autorun'
require 'minitest/pride'

require 'canvas'

class TestCanvasFill < Minitest::Test
  def setup
    @canvas = Canvas.new(5,4)
    out=<<EOL
OOOOO
OOOOO
OOOOO
OOOOO
EOL
    assert_output(out) { @canvas.draw }
  end

  def test_fill_a_clear_canvas_will_fill_everything
    @canvas.fill(1,1,'Y')
    out=<<EOL
YYYYY
YYYYY
YYYYY
YYYYY
EOL
    assert_output(out) { @canvas.draw }
  end

  def test_fill_a_canvas_with_some_shapes_will_leave_the_shapes_untouched
    @canvas.pixel(3,3,'X')

    @canvas.fill(1,1,'E')

    out=<<EOL
EEEEE
EEEEE
EEXEE
EEEEE
EOL
    assert_output(out) { @canvas.draw }
  end

  def test_fill_a_canvas_with_some_closed_shapes_will_leave_the_non_contiguous_areas_untouched
    @canvas.rect(1,3,3,3,'E')
    @canvas.rect(3,1,3,3,'E')

    @canvas.fill(1,1,'I')

    out=<<EOL
IIEOO
IIEOO
EEEOO
OOOOO
EOL
    assert_output(out) { @canvas.draw }

    @canvas.fill(1,3,'A')

    out=<<EOL
IIAOO
IIAOO
AAAOO
OOOOO
EOL
    assert_output(out) { @canvas.draw }
  end

  def test_fill_should_only_accept_integer_x_and_y
    exception = assert_raises(ArgumentError) { @canvas.fill('x',2,'Y') }
    assert_equal "x must be an integer", exception.message

    exception = assert_raises(ArgumentError) { @canvas.fill(2,'y','Y') }
    assert_equal "y must be an integer", exception.message
  end

  def test_fill_will_not_accept_out_of_boundary_x_or_y
    exception = assert_raises(RangeError) { @canvas.fill(7,2,'Y') }
    assert_equal "x must be between 1 and 5", exception.message

    exception = assert_raises(RangeError) { @canvas.fill(2,7,'Y') }
    assert_equal "y must be between 1 and 4", exception.message

    exception = assert_raises(RangeError) { @canvas.fill(0,2,'Y') }
    assert_equal "x must be between 1 and 5", exception.message

    exception = assert_raises(RangeError) { @canvas.fill(2,0,'Y') }
    assert_equal "y must be between 1 and 4", exception.message
  end

  def test_fill_will_take_only_single_capital_letter_as_colour
    exception = assert_raises(ArgumentError) { @canvas.fill(2,2,'Yellow') }
    assert_equal "colour must be a single capital letter", exception.message

    exception = assert_raises(ArgumentError) {  @canvas.fill(2,2,2) }
    assert_equal "colour must be a single capital letter", exception.message
  end
end
