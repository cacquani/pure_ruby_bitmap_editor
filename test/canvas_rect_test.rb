require 'minitest/autorun'
require 'minitest/pride'

require 'canvas'

class TestCanvasRect < Minitest::Test
  def setup
    @canvas = Canvas.new(4,5)
  end

  def test_can_draw_a_rectangle
    @canvas.rect(2,2,3,4,'Y')
    out=<<EOL
OOOO
OYYO
OYYO
OYYO
OOOO
EOL
    assert_output(out) { @canvas.draw }
  end

  def test_rect_will_accept_only_integer_corners
    exception = assert_raises(ArgumentError) { @canvas.rect('x',2,3,3,'Y') }
    assert_equal "x1 must be an integer", exception.message

    exception = assert_raises(ArgumentError) { @canvas.rect(2,'y',3,3,'Y') }
    assert_equal "y1 must be an integer", exception.message

    exception = assert_raises(ArgumentError) { @canvas.rect(2,2,'x',3,'Y') }
    assert_equal "x2 must be an integer", exception.message

    exception = assert_raises(ArgumentError) { @canvas.rect(2,2,3,'y','Y') }
    assert_equal "y2 must be an integer", exception.message
  end

  def test_colour_must_be_a_single_capital_letter
    exception = assert_raises(ArgumentError) { @canvas.rect(2,2,3,3,'Yellow') }
    assert_equal "colour must be a single capital letter", exception.message

    exception = assert_raises(ArgumentError) { @canvas.rect(2,2,3,3,2) }
    assert_equal "colour must be a single capital letter", exception.message
  end

  def test_order_of_params_shouldnt_matter
    @canvas.rect(3,4,2,2,'Y')
    out=<<EOL
OOOO
OYYO
OYYO
OYYO
OOOO
EOL
    assert_output(out) { @canvas.draw }

    @canvas.clear

    @canvas.rect(4,4,1,2,'Y')
    out=<<EOL
OOOO
YYYY
YYYY
YYYY
OOOO
EOL
    assert_output(out) { @canvas.draw }
  end

  def test_can_draw_a_vline
    canvas = Canvas.new(3,3)
    canvas.rect(2,1,2,2,'R')
    out=<<EOL
ORO
ORO
OOO
EOL
    assert_output(out) { canvas.draw }
  end

  def test_can_draw_a_hline
    canvas = Canvas.new(3,3)
    canvas.rect(1,2,2,2,'R')
    out=<<EOL
OOO
RRO
OOO
EOL
    assert_output(out) { canvas.draw }
  end
end
