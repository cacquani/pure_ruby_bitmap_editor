require 'minitest/autorun'
require 'minitest/pride'

require 'canvas'

class TestCanvasPixel < Minitest::Test
  def test_can_colour_a_pixel
    canvas = Canvas.new(1,1)
    canvas.pixel(1,1,'B')
    out=<<EOL
B
EOL
    assert_output(out) { canvas.draw }
  end

  def test_can_colour_the_right_pixel
    canvas = Canvas.new(4,3)
    canvas.pixel(1,1,'B')
    out=<<EOL
BOOO
OOOO
OOOO
EOL
    assert_output(out) { canvas.draw }

    canvas.pixel(2,3,'Y')
    out=<<EOL
BOOO
OOOO
OYOO
EOL
    assert_output(out) { canvas.draw }
  end

  def test_pixel_will_take_only_integer_x_and_y
    canvas = Canvas.new(4, 4)

    exception = assert_raises(ArgumentError) { canvas.pixel('x',2,'Y') }
    assert_equal "x must be an integer", exception.message

    exception = assert_raises(ArgumentError) { canvas.pixel(2,'y','Y') }
    assert_equal "y must be an integer", exception.message

    exception = assert_raises(ArgumentError) { canvas.pixel('x','y','Y') }
    assert_equal "x must be an integer, y must be an integer", exception.message      
  end

  def test_pixel_will_take_only_single_capital_letter_as_colour
    canvas = Canvas.new(4, 4)

    exception = assert_raises(ArgumentError) { canvas.pixel(2,2,'Yellow') }
    assert_equal "colour must be a single capital letter", exception.message

    exception = assert_raises(ArgumentError) {  canvas.pixel(2,2,2) }
    assert_equal "colour must be a single capital letter", exception.message
  end

  def test_pixel_will_not_accept_out_of_boundary_x_or_y
    canvas = Canvas.new(4, 3)

    exception = assert_raises(RangeError) { canvas.pixel(7,2,'Y') }
    assert_equal "x must be between 1 and 4", exception.message

    exception = assert_raises(RangeError) { canvas.pixel(2,7,'Y') }
    assert_equal "y must be between 1 and 3", exception.message

    exception = assert_raises(RangeError) { canvas.pixel(-1,2,'Y') }
    assert_equal "x must be between 1 and 4", exception.message

    exception = assert_raises(RangeError) { canvas.pixel(2,-1,'Y') }
    assert_equal "y must be between 1 and 3", exception.message

    exception = assert_raises(RangeError) { canvas.pixel(0,2,'Y') }
    assert_equal "x must be between 1 and 4", exception.message

    exception = assert_raises(RangeError) { canvas.pixel(2,0,'Y') }
    assert_equal "y must be between 1 and 3", exception.message
  end
end
