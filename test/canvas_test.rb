require 'minitest/autorun'
require 'minitest/spec'
require 'minitest/pride'

require 'canvas'

class TestCanvas < Minitest::Test
  def test_canvas_is_not_created_without_size
    exception = assert_raises(ArgumentError) { canvas = Canvas.new }
  end

  def test_canvas_is_created
    canvas = Canvas.new(1,1)
    assert_kind_of Canvas, canvas
  end

  def test_canvas_return_significant_error_when_m_or_n_not_integer
    exception = assert_raises(ArgumentError) { Canvas.new('m', 2) }
    assert_equal "Width must be an integer", exception.message

    exception = assert_raises(ArgumentError) {  Canvas.new(2, 'n') }
    assert_equal "Height must be an integer", exception.message

    exception = assert_raises(ArgumentError) { Canvas.new('m', 'n') }
    assert_equal "Width must be an integer, Height must be an integer", exception.message
  end

  def test_new_canvas_is_white
    canvas = Canvas.new(1,1)
    assert_output(/^O$/) { canvas.draw }
  end

  def test_new_canvas_is_correct_size
    canvas = Canvas.new(3,4)
    out=<<EOL
OOO
OOO
OOO
OOO
EOL
    assert_output(out) { canvas.draw }

    canvas = Canvas.new(4,3)
    out=<<EOL
OOOO
OOOO
OOOO
EOL
    assert_output(out) { canvas.draw }
  end

  def test_when_cleaning_canvas_it_is_white
    canvas = Canvas.new(3,4)
    out=<<EOL
OOO
OOO
OOO
OOO
EOL
    assert_output(out) { canvas.draw }

    canvas.pixel(2,2,'R')
    canvas.pixel(2,3,'R')

    out=<<EOL
OOO
ORO
ORO
OOO
EOL
    assert_output(out) { canvas.draw }

    canvas.clear
    out=<<EOL
OOO
OOO
OOO
OOO
EOL
    assert_output(out) { canvas.draw }
  end
end
